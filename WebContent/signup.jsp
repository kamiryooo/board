<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
		<a href="management">戻る</a> <br />

		<h3>ユーザー新規登録</h3>

			  <br /><label for="name">名前</label> <input name="name" id="name" value="${name}"/>*10文字以内<br />

			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" value="${password}"/>*6文字以上20文字以内<br />
			<label for="passwordcheck">確認用パスワード</label>
			<input name="passwordcheck" type="password" id="passwordcheck" value="${passwordcheck}"/>
			<br />
			<label for="id">ID</label> <input name="login_id" id="login_id" value="${login_id }"/>*6文字以上20文字以内 <br />

			<select name="branch">
				<c:if test="${users.branch == branches.id}">
					<option value="1">東京本社</option>
					<option value="2">大阪支店</option>
					<option value="3">名古屋支店</option>
					<option value="4">ホノルル支店</option>
				</c:if>
			</select><br />

			<c:if test="${users.position == positions.id}">
				<select name="position"><option value="1">総務部</option>
					<option value="2">管理部</option>
					<option value="3">人事部</option>
					<option value="4">営業部</option>
				</select>
			</c:if>

			<!-- <label for="branch">支店</label> <input
				name="branch" id="branch" /> <br /> <label for="position">部署・役職</label>
			<input name="position" id="position" /> <br />
 -->
			 <br /><input type="submit" value="登録" /> <br />
		</form>
		<br />
		<br />
		<br />

		<div class="copyright">Copyright(c)Yuta Kamiryo</div>
	</div>

</body>
</html>