<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<link rel="stylesheet"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/UI lightness/jquery-ui.css">
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
<script>
	$(function() {
		$("#date").datepicker();
	});
</script>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>掲示板</title>
</head>
<body>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<div class="header">
		<c:if test="${ empty loginUser }">
			<a href="login">ログイン</a>
			<a href="signup">登録する</a>
		</c:if>
		<c:if test="${ not empty loginUser }">
		<c:if test="${ loginUser.position == 1 }">
			<a href="management">ユーザー管理</a>
		</c:if>
			<a href="contribution">新規投稿へ</a>
			<a href="logout">ログアウト</a>
		</c:if>
	</div>
	<br />
	<form method="get">
		<p>カテゴリー</p>
		<input type="search" placeholder="キーワードを入力" name="category" id="category" value="${category}">

		<p>期間</p>
		<input type="search" name="from" id="datepicker" value="${from}"> ～
		<input type="search" name="to" id="date" value="${to}"> <input type="submit" name="submit" value="検索">
	</form>
	<br />

	<c:if test="${ not empty loginUser }">
		<div class="profile"></div>
	</c:if>
	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<div class="message">
				<br /><br /><br />
				<div class="name">
					<span class="name"><c:out value="${message.name}" /></span>
				</div>
				<br />
				<div class="title">
					<span class="title"><c:out value="${message.title}" /></span>
				</div>
				<br />
				<div class="category">
					<c:out value="${message.category}" />
				</div>
				<br />
				<c:forEach var="str" items="${ fn:split(message.text,'
				')}" >
				${str}<br>
				<div class="text">
					<c:out value="${text}" />
				</div>
				</c:forEach>
				<br />
				<div class="date">
					<fmt:formatDate value="${message.created_date}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
				<c:if test="${message.user_id == loginUser.id}">
					<form action="delete" method="post">
						<input type="hidden" name="message_id" value="${message.id}">
						<input type="submit" value="投稿削除">
					</form>
				</c:if>
			</div>
			<br />
			<br />
				<c:forEach items="${comments}" var="comment">
					<c:if test="${message.id == comment.contribution}">
						<br />
						<c:out value="${comment.name}" />
						<br />
						<c:out value="${comment.text}" />
						<div class="date">
							<fmt:formatDate value="${message.created_date}"
								pattern="yyyy/MM/dd HH:mm:ss" />
							<br />
							<form action="commentdelete" method="post">
							<c:if test="${comment.user_id == loginUser.id}">
								<input type="hidden" name="comment_id" value="${comment.id}">
								<input type="submit" value="コメント削除">
							</c:if>
							</form>
						</div>
					</c:if>
					<%-- <c:if test="${comment.user_id == loginUser.id}">
						<form action="commentdelete" method="post">
							<input type="hidden" name="comment_id" value="${comment.id}">
							<input type="submit" value="コメント削除">
						</form>
					</c:if> --%>
				</c:forEach>
				<div class="form-area">
				<form action="comment" method="post">
					<textarea name="comment" cols="100" rows="5" class="text"></textarea>
					<input type="hidden" name="contribution" value="${message.id}">
					<br /> <input type="submit" value="コメント">*500文字以内 <br />
				</form>

			</div>
		</c:forEach>
	</div>
</body>
</html>