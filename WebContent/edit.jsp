<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー情報編集</title>
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="edit" method="post">
		<a href="management">戻る</a><br />
		<h3>ユーザー編集</h3>
			<br /> <input name="id" value="${editUser.id}" id="id" type="hidden" />

			<label for="name">名前</label>
			<input name="name" value="${editUser.name}" id="name" />*10文字以内<br />

			<label for="password">パスワード</label>
			<input name="password" type="password" id="password" />*6文字以上20文字以内 <br />

			<label for="passwordcheck">パスワード(確認用)</label>
			<input name="passwordcheck" type="password" id="passwordcheck" /> <br />

			<label for="login_id">ID</label>
			<input name="login_id" id="login_id" value="${editUser.login_id}" />*6文字以上20文字以内
			<br />

			<c:if test="${loginUser.id != editUser.id}">
			<label for="branch">支店</label>
			<select name="branch">
				<c:if test="${users.branch == branches.id}">
					<option value="1">東京本社</option>
					<option value="2">大阪支店</option>
					<option value="3">名古屋支店</option>
					<option value="4">ホノルル支店</option>
				</c:if>
			</select><br />

			<c:if test="${users.position == positions.id}">
				<label for="position">部署・役職</label>
				<select name="position"><option value="1">総務部</option>
					<option value="2">管理部</option>
					<option value="3">人事部</option>
					<option value="4">営業部</option>
				</select>
				<br />
			</c:if>
			</c:if>

			<c:if test="${loginUser.id == editUser.id}">
				<select name="branch">
					<c:if test="${users.branch == branches.id}">
						<option value="1">東京本社</option>
					</c:if>
				</select><br />

			<c:if test="${users.position == positions.id}">
				<select name="position">
					<option value="1">総務部</option>
				</select>
				<br />
			</c:if>
			</c:if>


			<!-- <label for="branch">支店</label>
                <input name="branch" value="${editUser.branch}" id="branch"/> <br />

                <label for="position">部署・役職</label>
                 <input name="position" value="${editUser.position}" id="position"/> <br />
                 -->

			<input type="submit" value="登録" /> <br />
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>