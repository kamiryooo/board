<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript">

function disp(){
	var result = window.confirm('本当に変更しますか？');

	if(result){

		return true;

	}
	else {

		return false;
	}
}
</script>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>

	<a href="./">戻る</a>
	<a href="signup">ユーザー新規登録</a><br />

	<h3>ユーザー管理</h3>
	<table>
		<tr>
			<th>名前</th>
			<th>ログインID</th>
			<th>支店</th>
			<th>役職</th>
			<th>停止・復活</th>
		</tr>

		<c:forEach items="${management}" var="userinfo">
			<tr>
				<td><c:out value="${userinfo.name}" /></td>
				<td><c:out value="${userinfo.login_id}" /></td>
				<td><c:out value="${userinfo.branches_name}" /></td>
				<td><c:out value="${userinfo.positions_name}" /></td>


				<td><c:if test="${loginUser.id == userinfo.id}">
					<a>ログイン中</a>
					</c:if>
					<c:if test="${loginUser.id != userinfo.id}">
				 <c:if test="${userinfo.isDeleted == 0}">
						<form action="management" method="post">
							<input type="hidden" name="userinfo" value="${userinfo.id}">
							<input type="hidden" name="isDeleted" value="0">
							<input type="submit" value="停止" onClick="return disp();">
						</form>
					</c:if> <c:if test="${userinfo.isDeleted == 1}">
						<form action="management" method="post">
							<input type="hidden" name="userinfo" value="${userinfo.id}">
							<input type="hidden" name="isDeleted" value="1"> <input
								type="submit" value="復活">
						</form>
					</c:if>
					</c:if>
					</td>
				<td><a href="edit?edit_id=${userinfo.id}">編集</a></td>
			</tr>
		</c:forEach>
	</table>
	<br />
	<br />
	<br />

	<div class="copyright">Copyright(c)Yuta Kamiryo</div>

</body>
</html>