<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<body>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />

	</c:if>
	<div class="form-area">
	<a href="./">戻る</a><br /><br />
		<form action="contribution" method="post">
		<h3>新規投稿</h3>
			<label for="title">タイトル</label> <input name="title" id="title" value="${title}"/>*30文字以内 <br />
			<label for="category">カテゴリー</label> <input name="category" id="category" value="${category}"/>*10文字以内 <br />
			 <br /><label for="text">本文</label>*1000文字以内 <br />
			<textarea name="message" id="message" ${message} cols="100" rows="5" class="tweet-box">${message}</textarea>
			<br /> <input type="submit" value="新規投稿">
		</form>
	</div>
</body>
</html>