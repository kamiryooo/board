package board.beans;

import java.io.Serializable;

public class UserManagement implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String login_id;
    private String name;
    private int position;
    private int branch;
    private int isDeleted;
    private String branches_name;
    private String positions_name;


    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	public String getBranches_name() {
		return branches_name;
	}
	public void setBranches_name(String branches_name) {
		this.branches_name = branches_name;
	}
	public String getPositions_name() {
		return positions_name;
	}
	public void setPositions_name(String positions_name) {
		this.positions_name = positions_name;
	}

}