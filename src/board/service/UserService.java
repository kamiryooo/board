package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import board.beans.User;
import board.beans.UserManagement;
import board.dao.UserDao;
import board.dao.UserPositionBranchDao;
import board.utils.CipherUtil;

public class UserService {

	private static final int LIMIT_NUM = 1000;

	public void register(User user) {


		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}public User getUser(int userId) {

		System.out.println(userId);

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, userId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void update(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);


			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void register(UserManagement management) {

		System.out.println(management);


		Connection connection = null;
		try {
			connection = getConnection();


			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public List<UserManagement> getUserManagement() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserPositionBranchDao userPositionBranchDao = new UserPositionBranchDao();
			List<UserManagement> management1 = userPositionBranchDao.getUserManagement(connection , LIMIT_NUM);

			commit(connection);

			return management1;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void isDeleted(int userid, int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserPositionBranchDao userPositionBranchDao = new UserPositionBranchDao();
			userPositionBranchDao.isDeleted(connection, userid, id);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}



}