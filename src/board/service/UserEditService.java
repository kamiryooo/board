package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;

import board.beans.UserEdit;
import board.dao.UserEditDao;
import board.utils.CipherUtil;

public class UserEditService {

	private static final int LIMIT_NUM = 1000;

	public void register(UserEdit userEdit) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(userEdit.getPassword());
			userEdit.setPassword(encPassword);

			UserEditDao userEditDao = new UserEditDao();
			userEditDao.insert(connection, userEdit);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public UserEdit getUserEdit(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserEditDao userEditDao = new UserEditDao();
			UserEdit userEdit = userEditDao.getUserEdit(connection, userId);

			commit(connection);

			return userEdit;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void update(UserEdit userEdit) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(userEdit.getPassword());
			userEdit.setPassword(encPassword);


			UserEditDao userEditDao = new UserEditDao();
			userEditDao.update(connection, userEdit);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public UserEdit getUserEdit() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserEditDao userEditDao = new UserEditDao();
			UserEdit userEdit = userEditDao.getUserEdit(connection , LIMIT_NUM);

			commit(connection);

			return userEdit;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
