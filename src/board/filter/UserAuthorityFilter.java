package board.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.User;

@WebFilter({"/edit", "/management", "/signup"})

public class UserAuthorityFilter implements Filter{
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

		System.out.println("権限チェック");

		// セッションが存在しない場合NULLを返す
		HttpSession session = ((HttpServletRequest)req).getSession();

		User loginUser = (User) session.getAttribute("loginUser");
		System.out.println("aaa");
		if(loginUser.getPosition().equals("1")) {
			System.out.println("bbb");

			chain.doFilter(req, res);
			return;
		}

		System.out.println("bbb");
		List<String> messages = new ArrayList<String>();
		messages.add("権限がありません");
		session.setAttribute("errorMessages", messages);
		System.out.println("bbb");
		((HttpServletResponse)res).sendRedirect("./");
	}

	@Override
	public void init(FilterConfig paramFilterConfig) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ

	}
}
