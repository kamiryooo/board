package board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.service.MessageService;

/**
 * Servlet implementation class ContibutionServlet
 */
@WebServlet(urlPatterns = { "/delete" })
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		int message_id = Integer.parseInt(request.getParameter("message_id"));

		MessageService messageService = new MessageService();
		messageService.delete(message_id);

		response.sendRedirect("./");

	}

}



