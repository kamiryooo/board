package board.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Message;
import board.beans.User;
import board.beans.UserComment;
import board.beans.UserMessage;
import board.service.CommentService;
import board.service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		//User user = (User) request.getSession().getAttribute("loginUser");

		//String a = "";
		//String b = "2018-05-01";
		//String c = "2018-06-01";

		String category = request.getParameter("category");
		String from = request.getParameter("from");
		String to = request.getParameter("to");

		if(category == null || category.length() == 0){
			category ="" ;
		}

		Date date = new Date();
		System.out.println(date);

		SimpleDateFormat d1 = new SimpleDateFormat("yyyy/MM/dd");
		String currentDate = d1.format(date);
		/*System.out.println(currentDate);

		System.out.println(from);
		System.out.println(to);*/
		if(from == null || from.length() == 0){
			from = "";
		}
		if(to == null || to.length() == 0){
			to = currentDate;
		}



		List<UserMessage> messages = new MessageService().getMessage(category, from, to);


		request.setAttribute("messages", messages);


		List<UserComment> comments = new CommentService().getComments();

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);

		request.setAttribute("category",category);
		request.setAttribute("from",from);
		request.setAttribute("to",to);

		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();
		//List<String> comments = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setTitle(request.getParameter("title"));
			message.setText(request.getParameter("message"));
			message.setCategory(request.getParameter("category"));
			message.setUser_id(user.getId());

			new MessageService().register(message);


			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);

			response.sendRedirect("./");
		}
	}
	private boolean isValid(HttpServletRequest request, List<String> comments) {

		String text = request.getParameter("comment");

		if (StringUtils.isBlank(text) == true) {
			comments.add("入力してください");
		}
		if (500 < text.length()) {
			comments.add("500文字以下で入力してください");
		}
		if (comments.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}