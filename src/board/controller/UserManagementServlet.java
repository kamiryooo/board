package board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.beans.UserManagement;
import board.service.UserService;


@WebServlet(urlPatterns = { "/management" })
public class UserManagementServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<UserManagement> management = new UserService().getUserManagement();

		request.setAttribute("management", management);

		request.getRequestDispatcher("management.jsp").forward(request, response);


	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		String isDeleted = request.getParameter("isDeleted");
		int userid = Integer.parseInt(request.getParameter("userinfo"));

		if(isDeleted != null && isDeleted.equals("0")) {
			int id = Integer.parseInt(request.getParameter("isDeleted"));
			new UserService().isDeleted(userid, id);
		}else if(isDeleted != null && isDeleted.equals("1")) {
			int id = Integer.parseInt(request.getParameter("isDeleted"));
			new UserService().isDeleted(userid, id);
		}

		response.sendRedirect("management");

	}

	/*		private boolean isValid(HttpServletRequest request, List<String> messages) {
			String name = request.getParameter("name");
			String password = request.getParameter("password");
			String passwordcheck = request.getParameter("passwordcheck");
			String login_id = request.getParameter("login_id");

			if (messages.size() == 0) {
				return true;
			} else {
				return false;
			}

	 */
}
