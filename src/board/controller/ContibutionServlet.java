package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Message;
import board.beans.User;
import board.service.MessageService;

/**
 * Servlet implementation class ContibutionServlet
 */
@WebServlet(urlPatterns = { "/contribution" })
public class ContibutionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("contribution.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String message2 = request.getParameter("message");
		String title = request.getParameter("title");
		String category = request.getParameter("category");

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setTitle(request.getParameter("title"));
			message.setText(request.getParameter("message"));
			message.setCategory(request.getParameter("category"));
			message.setUser_id(user.getId());

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);

			request.setAttribute("category",category);
			request.setAttribute("title",title);
			request.setAttribute("message",message2);

			request.getRequestDispatcher("contribution.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String message = request.getParameter("message");
		String title = request.getParameter("title");
		String category = request.getParameter("category");

		if (StringUtils.isBlank(title) == true) {
			messages.add("タイトルを入力してください");
		}
		if (30 < title.length()) {
			messages.add("タイトルを30文字以下で入力してください");
		}
		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (10 < category.length()) {
			messages.add("カテゴリーを10文字以下で入力してください");
		}
		if (StringUtils.isBlank(message) == true) {
			messages.add("本文を入力してください");
		}
		if (1000 < message.length()) {
			messages.add("本文を1000文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
