package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.User;
import board.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String passwordcheck = request.getParameter("passwordcheck");
		String login_id = request.getParameter("login_id");

		HttpSession session = request.getSession();

		if (isValid(request, messages) == true) {

			User user = new User();
			user.setName(request.getParameter("name"));
			user.setPassword(request.getParameter("password"));
			user.setPassword(request.getParameter("passwordcheck"));
			user.setLogin_id(request.getParameter("login_id"));
			user.setPosition(request.getParameter("position"));
			user.setBranch(request.getParameter("branch"));

			new UserService().register(user);

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("name",name);
			request.setAttribute("password",password);
			request.setAttribute("passwordcheck",passwordcheck);
			request.setAttribute("login_id",login_id);

			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String passwordcheck = request.getParameter("passwordcheck");
		String login_id = request.getParameter("login_id");
		String position = request.getParameter("position");
		String branch = request.getParameter("branch");

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}
		if(name.length() > 10) {
			messages.add("名前は10文字以内で入力して下さい");
		}
		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}else if(!(password.length() >= 6 && password.length() <= 20)){
			messages.add("パスワードは6文字以上20文字以内で入力して下さい");
		}
		if(!(password.matches("[azAZ0*9]"))){
		     System.out.println("パスワードは半角英数記号で入力してください");
		}
		if(!password.equals(passwordcheck)) {
			messages.add("パスワードが一致しません");
		}
		if (StringUtils.isBlank(login_id) == true) {
			messages.add("IDを入力してください");
		} else if(!(login_id.length() >= 6 && login_id.length() <= 20)) {
			messages.add("IDは6文字以上20文字以内で入力して下さい");
		}
		/*if(!(login_id.matches("[azAZ0*9]"))) {
			messages.add("IDは半角英数字で入力してください");
		}*/
		if (StringUtils.isBlank(branch) == true) {
			messages.add("支店を入力してください");
		}
		if (StringUtils.isBlank(position) == true) {
			messages.add("部署・役職を入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}