package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.User;
import board.beans.UserEdit;
import board.service.UserEditService;
import board.service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class UserEditServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		int id = Integer.parseInt(request.getParameter("edit_id"));


		System.out.println("edit_id:" + id);

		User editUser = new UserService().getUser(id);

		//if(id

		request.setAttribute("editUser", editUser);
		System.out.println(editUser);

		request.getRequestDispatcher("edit.jsp").forward(request, response);

		}

	//}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		if (isValid(request, messages) == true) {

			UserEdit userEdit = new UserEdit();
			userEdit.setId(Integer.parseInt(request.getParameter("id")));
			userEdit.setName(request.getParameter("name"));
			userEdit.setPassword(request.getParameter("password"));
			userEdit.setPassword(request.getParameter("passwordcheck"));
			userEdit.setLogin_id(request.getParameter("login_id"));
			userEdit.setPosition(request.getParameter("position"));
			userEdit.setBranch(request.getParameter("branch"));

			new UserEditService().update(userEdit);

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("edit");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String passwordcheck = request.getParameter("passwordcheck");
		String login_id = request.getParameter("login_id");

		/*for(String login_id : messages) {
			if(messages.contains(login_id)) {
				System.out.println("IDが重複しています");
			}
		}*/
		if (name.length() > 10) {
			messages.add("名前は10文字以内で入力してください");
		}
		/*if(!(password.length() < 6 && password.length() > 20)){
			messages.add("パスワードは6文字以上20文字以内で入力して下さい");
		}*/
		if (!password.equals(passwordcheck)) {
			messages.add("パスワードが一致しません");
		}
		if (StringUtils.isBlank(login_id) == true) {
			messages.add("IDを入力してください");
		} else if(!(login_id.length() >= 6 && login_id.length() <= 20)) {
			messages.add("IDは6文字以上20文字以内で入力して下さい");
		}
		/*if(!(login_id.contains("[azAZ0*9]"))) {
			messages.add("IDは半角英数字で入力してください");
		}*/
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
