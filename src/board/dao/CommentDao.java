package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import board.beans.Comment;
import board.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append(" text");
            sql.append(", created_date");
            sql.append(", user_id");
            sql.append(", contribution");
            sql.append(") VALUES (");
            sql.append(" ?"); //user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?");
            sql.append(", ?");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, comment.getText());
            ps.setInt(2, comment.getUserId());
            ps.setString(3, comment.getContribution());


            System.out.println(ps);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void delete(Connection connection, int comment_id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments WHERE ");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment_id);

            System.out.println(ps);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
