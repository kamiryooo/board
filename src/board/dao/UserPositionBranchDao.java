package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import board.beans.UserManagement;
import board.exception.SQLRuntimeException;


public class UserPositionBranchDao {

	public List<UserManagement> getUserManagement(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("users.isDeleted as isDeleted, ");
			sql.append("branch, ");
			sql.append("position, ");
			sql.append("positions.name as positon_name, ");
			sql.append("branches.name as branch_name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position = positions.id ");

			ps = connection.prepareStatement(sql.toString());

			System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
			List<UserManagement> ret = toUserManagementList(rs);

			System.out.println(ps.executeQuery());

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserManagement> toUserManagementList(ResultSet rs)
			throws SQLException {

		List<UserManagement> ret = new ArrayList<UserManagement>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int position = rs.getInt("position");
				int isDeleted = rs.getInt("isDeleted");
				int branch = rs.getInt("branch");
				String branches_name = rs.getString("branch_name");
				String positions_name = rs.getString("positon_name");


				UserManagement management = new UserManagement();
				management.setId(id);
				management.setLogin_id(login_id);
				management.setName(name);
				management.setPosition(position);
				management.setIsDeleted(isDeleted);
				management.setBranch(branch);
				management.setBranches_name(branches_name);
				management.setPositions_name(positions_name);


				ret.add(management);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public void isDeleted(Connection connection, int userid, int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			if(id == 1) {
				sql.append("isDeleted = 0 ");
			}
			else if(id == 0) {
				sql.append("isDeleted = 1 ");
			}
			sql.append("WHERE ");
			sql.append("id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, userid);



			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
