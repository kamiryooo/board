package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.UserMessage;
import board.exception.SQLRuntimeException;


public class UserMessageDao {

	public List<UserMessage> getUserMessages(Connection connection, String category, String from, String to) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("contribution.id as id, ");
			sql.append("contribution.title as title, ");
			sql.append("contribution.category as category, ");
			sql.append("contribution.text as text, ");
			sql.append("contribution.user_id as user_id, ");
			sql.append("users.name as name, ");
			sql.append("contribution.created_date as created_date ");
			sql.append("FROM contribution ");
			sql.append("INNER JOIN users ");
			sql.append("ON contribution.user_id = users.id ");
			sql.append("where contribution.created_date >= ? and contribution.created_date <= ?");
			if(!(category.isEmpty())) {
				sql.append("and contribution.category LIKE ?");
			}
			sql.append("ORDER BY created_date DESC  ");

			ps = connection.prepareStatement(sql.toString());

			;
			System.out.println(category);

			ps.setString(1, from + " 00:00:00");
			ps.setString(2, to + " 23:59:59");
			if(!(category.isEmpty())) {
				ps.setString(3, "%" + category + "%");
			}

			System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setName(name);
				message.setId(id);
				message.setUser_id(user_id);
				message.setTitle(title);
				message.setCategory(category);
				message.setText(text);
				message.setCreated_date(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
