package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.UserComment;
import board.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.created_date as created_date, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.contribution as contribution, ");
			sql.append("comments.text as text, ");
			sql.append("users.name as name ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentsList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toUserCommentsList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				Timestamp createdDate = rs.getTimestamp("created_date");
				int user_id = rs.getInt("user_id");
				String contribution = rs.getString("contribution");
				String text = rs.getString("text");
				String name = rs.getString("name");

				UserComment comments = new UserComment();
				comments.setId(id);
				comments.setCreated_date(createdDate);
				comments.setUser_id(user_id);
				comments.setContribution(contribution);
				comments.setText(text);
				comments.setName(name);




				ret.add(comments);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
