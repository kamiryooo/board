package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.UserEdit;
import board.exception.SQLRuntimeException;

public class UserEditDao {

	public void update(Connection connection, UserEdit UserEdit) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" name = ?");
			sql.append(", password = ?");
			sql.append(", login_id = ?");
			sql.append(", position = ?");
			//sql.append(", isDeleted");
			sql.append(", branch = ?");
			//sql.append(", created_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, UserEdit.getName());
			ps.setString(2, UserEdit.getPassword());
			ps.setString(3, UserEdit.getLogin_id());
			ps.setString(4, UserEdit.getPosition());
			ps.setString(5, UserEdit.getBranch());
			//ps.setString(5, UserEdit.getIsDeleted());
			ps.setInt(6, UserEdit.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void insert(Connection connection, UserEdit userEdit) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append(" name");
			sql.append(", password");
			sql.append(", login_id");
			sql.append(", position");
			sql.append(", branch");
			sql.append(", isDeleted");
			sql.append(", created_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // name
			sql.append(", ?"); // password
			sql.append(", ?"); // id
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", 0");
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, userEdit.getName());
			ps.setString(2, userEdit.getPassword());
			ps.setString(3, userEdit.getLogin_id());
			ps.setString(4, userEdit.getPosition());
			ps.setString(5, userEdit.getBranch());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public UserEdit getUserEdit(Connection connection,String login_id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, login_id);

			ResultSet rs = ps.executeQuery();
			List<UserEdit> userEditList = toUserEditList(rs);
			if (userEditList.isEmpty() == true) {
				return null;
			} else if (2 <= userEditList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userEditList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<UserEdit> toUserEditList(ResultSet rs) throws SQLException {

		List<UserEdit> ret = new ArrayList<UserEdit>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String password = rs.getString("password");
				String login_id = rs.getString("login_id");
				String position = rs.getString("position");
				String isDeleted = rs.getString("isDeleted");
				String branch = rs.getString("branch");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserEdit userEdit = new UserEdit();
				userEdit.setId(id);
				userEdit.setName(name);
				userEdit.setPassword(password);
				userEdit.setLogin_id(login_id);
				userEdit.setPosition(position);
				userEdit.setIsDeleted(isDeleted);
				userEdit.setBranch(branch);
				userEdit.setCreatedDate(createdDate);

				ret.add(userEdit);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public UserEdit getUserEdit(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<UserEdit> userEditList = toUserEditList(rs);
			if (userEditList.isEmpty() == true) {
				return null;
			} else if (2 <= userEditList.size()) {
				throw new IllegalStateException("2 <= userEditList.size()");
			} else {
				return userEditList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public UserEdit getUserEdit(Connection connection, String login_id, String password) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<UserEdit> userEditList = toUserEditList(rs);
			if (userEditList.isEmpty() == true) {
				return null;
			} else if (2 <= userEditList.size()) {
				throw new IllegalStateException("2 <= userEditList.size()");
			} else {
				return userEditList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
